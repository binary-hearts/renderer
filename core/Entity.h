#ifndef OBJECTS_H
#define OBJECTS_H

#include<glm/vec3.hpp>
#include <vector>
#include "texture.h"
#include "Shader.h"

enum class ObjectType{MESH=1,LIGHT,CAMERA};

class Entity
{
    public:
        Entity();
        virtual ~Entity();
        void setPosition(glm::vec3);
        void setRotation(glm::vec3);
        void setScale(glm::vec3);

        glm::vec3 getPosition();
        glm::vec3 getRotation();
        glm::vec3 getScale();

        //Virtual functions
        virtual ObjectType getObjectType() = 0;
        virtual void update(float delta){};
        virtual void draw(){}

        void addChild(Entity*);
        void removeChild(Entity*);
        bool isHidden;
        int ID;
        inline static int LargestID;
        Shader* shader = 0;


    protected:
        glm::vec3 m_position;
        glm::vec3 m_rotation;
        glm::vec3 m_scale;


    private:

        Entity* m_parent;
        std::vector<Entity*> m_children;
};


#endif // OBJECTS_H

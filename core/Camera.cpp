#include "Camera.h"

Camera::Camera(float width, float height,float FOV,Window* window, float yaw = 0, float pitch = 0)
: m_FOV(FOV)
, m_active(false)
, m_yaw(yaw)
, m_pitch(pitch)
, m_window(window)
{
    float aspect = width/height;

    m_position     = glm::vec3(0.0f, 0.0f, 0.0f);
    m_target       = glm::vec3(0.0f, 0.0f, 0.0f);
    m_up           = glm::vec3(0.0,0.1,0.0);
}

void
Camera::update(float delta){
}

glm::mat4
Camera::getProjectionMatrix(){
	float aspect = m_window->width/m_window->height;
	return glm::perspective(glm::radians(m_FOV),aspect,0.1f,10000.0f);
}

glm::mat4
Camera::getWorldToViewMatrix(){
    glm::quat orientation = glm::quat(glm::vec3(m_pitch, m_yaw, 0.0f));
    glm::vec3 front = glm::normalize(orientation * glm::vec3(0.0f, 0.0f, -1.0f));
    glm::vec3 worldUp(0.0f, 1.0f, 0.0f);
    glm::vec3 right = glm::normalize(glm::cross(front, worldUp));
    glm::vec3 up = glm::normalize(glm::cross(right, front));
    glm::mat4 viewMatrix = glm::lookAt(m_position, m_position + front, up);
    return viewMatrix;
}

void
Camera::activate(){
    m_active = true;
    if (Shader::CurrentlyBound != nullptr){
        Shader::CurrentlyBound->setMatrix4(glm::value_ptr(m_projection),"u_projection");
        m_view = this->getWorldToViewMatrix();
        Shader::CurrentlyBound->setMatrix4(glm::value_ptr(m_view),"u_view");
    }
}

void
Camera::moveForward(float distance){
    glm::quat orientation = glm::quat(glm::vec3(m_pitch, m_yaw, 0.0f));
    glm::vec3 front = glm::normalize(orientation * glm::vec3(0.0f, 0.0f, -1.0f));
    m_position += front*distance;
}

void
Camera::moveBackward(float distance){
    glm::quat orientation = glm::quat(glm::vec3(m_pitch, m_yaw, 0.0f));
    glm::vec3 front = glm::normalize(orientation * glm::vec3(0.0f, 0.0f, -1.0f));
    m_position -= front*distance;
}

void
Camera::moveLeft(float distance){
    glm::quat orientation = glm::quat(glm::vec3(m_pitch, m_yaw, 0.0f));
    glm::vec3 front = glm::normalize(orientation * glm::vec3(0.0f, 0.0f, -1.0f));
    glm::vec3 worldUp(0.0f, 1.0f, 0.0f);
    glm::vec3 right = glm::normalize(glm::cross(front, worldUp));

    m_position -= right*distance;
}

void
Camera::moveRight(float distance){
    glm::quat orientation = glm::quat(glm::vec3(m_pitch, m_yaw, 0.0f));
    glm::vec3 front = glm::normalize(orientation * glm::vec3(0.0f, 0.0f, -1.0f));
    glm::vec3 worldUp(0.0f, 1.0f, 0.0f);
    glm::vec3 right = glm::normalize(glm::cross(front, worldUp));

    m_position += right*distance;
}

void
Camera::moveUp(float distance){
	m_position.y += distance;
}

void
Camera::moveDown(float distance){
	m_position.y -= distance;
}

Camera::~Camera(){}



glm::vec3
Camera::getTarget(){
    return this->m_target;
}
void
Camera::setTarget(glm::vec3 target){
    this->m_target = target;
}

Window*& Camera::getWindow(){
	return m_window;
}

void Camera::setWindow(Window *mWindow) {
	m_window = mWindow;
}

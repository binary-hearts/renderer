#include <glm/glm.hpp>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <cerrno>
#include <error.h>
#include "BufferObject.h"
#include "Entity.h"
#include "Shader.h"


struct Vertex{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoord;
};

class Mesh : public Entity{

  public:
    std::vector<Vertex> vertices;
    Mesh(const char*);
    ~Mesh();

    void draw() override {
        GLCall(glBindVertexArray(m_VAO));
        GLCall(glDrawArrays(GL_TRIANGLES, 0, vertices.size()));
        GLCall(glBindVertexArray(0));
    }
    ObjectType getObjectType(){
        return ObjectType::MESH;
    }

    void AttachShader(Shader* shader){
    	this->shader = shader;
    }

  private:
    const char* m_filePath;
    FILE* m_file;
    void m_loadOBJ();
    void m_setupMesh();

    unsigned int m_VAO;
    VertexBuffer m_VBO;
};

#ifndef WINDOW_H
#define WINDOW_H
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <vector>
#include <functional>
#include "Log.h"
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

struct Inputfunction{
  int key;
  int action;
  std::function<void()> callFunction;
  Inputfunction(int key,int action,std::function<void()>call_function){
    this->key = key;
    this->action = action;
    this->callFunction = call_function;
  }
};



class Window
{
public:
    Window(int width,int height,char title[]);
    ~Window();
    bool isOpen();
    void swapBuffers(int interval = 1);
    float getTime();
    static void s_framebuffersize(GLFWwindow* window,int width,int height);
    void addInputFunction(int key,int action,std::function<void()> call_func);
    void addMouseAction(std::function<void(double,double)> call_func){
    	m_mouseActions.push_back(call_func);
    }
    void Info();
    GLFWwindow* m_window;
    bool mouseLocked;
    int  width,height;

protected:

private:
    std::vector<Inputfunction> m_inputs;
    std::vector<std::function<void(double,double)>> m_mouseActions;
    Log m_windowLog;
    char* m_title;

    //code organizers
    void callbacks();

    //statics
    static void s_errorcallback(int error, const char* description)
    {
        Log windowLog("./data/logs/glfw.log");
        windowLog.error(description);
    }
    static void s_keycallback(GLFWwindow* window, int key, int scancode, int action, int mods);

    static void s_mouseCursorCallback(GLFWwindow* window, double xpox, double ypos);

};

#endif // WINDOW_H

#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include <stdio.h>
#include <cstdlib>
#include <sys/stat.h>
#include <time.h>

#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <errno.h>

#include "Log.h"
#include "error.h"

#include "texture.h"


class Shader
{
    public:
        Shader(const char* path);
        ~Shader();
        void bindShader();
        void bindTexture();
        void unbindShader();
        void remove();
        void compile();
        GLuint ID;
        bool isRecompiled;
        inline static Shader* CurrentlyBound;

        //Uniforms
        void setFloat1(GLfloat value , const   char    name[]);
        void setFloat2(GLfloat value1, GLfloat value2 , const   char    name[]);
        void setFloat3(GLfloat value1, GLfloat value2 , GLfloat value3, const   char    name[]);
        void setFloat4(GLfloat value1, GLfloat value2 , GLfloat value3, GLfloat value4 , const  char  name[]);
        void setInt1  (GLint   value , const   char     name[]);
        void setInt2  (GLint   value1, GLint   value2 , const   char    name[]);
        void setInt3  (GLint   value1, GLint   value2 , GLint   value3, const   char     name[]);
        void setInt4  (GLint   value1, GLint   value2 , GLint   value3, GLint   value4 , const  char  name[]);
        void setMatrix2(GLfloat *value , const char    name[]);
        void setMatrix3(GLfloat *value , const char    name[]);
        void setMatrix4(GLfloat *value , const char    name[]);

        void attachTexture(Texture*);

    protected:
    	enum shadertype{
	    	VERTEX = 1,
		    FRAGMENT
	    };
        int m_bound = 0;
        std::ifstream m_shaderFile;

        void  m_read();

    private:
        std::vector<Texture*> m_textureList;
        void m_countSize();
        void m_compileShader(unsigned int shadertype,std::string& source);
        void m_addProgram();
	    shadertype m_getShaderType();
        time_t m_readOn;

        const char* m_shaderCodePath;
        std::string vertexsource;
        std::string fragmentsource;
        Log shaderlog;
        unsigned int m_vertexshader;
        unsigned int m_fragmentshader;
};

#endif // SHADER_H

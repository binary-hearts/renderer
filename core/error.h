#pragma once
#ifndef ERROR_H
#define ERROR_H
#include <stdio.h>
#include "glad/glad.h"
#include "Log.h"



static void GLClearError()
{
    while(glGetError()!=GL_NO_ERROR);
}
static bool GLLogCall(const char* function ,const char* file ,int line)
{
        while(GLenum error = glGetError())
        {
            printf("[OPENGL ERROR]:(%d) in [%s], File:%s at line(%d)\n",error,function,file,line);
            return false;
        }
        return true;
}
#define ASSERT(x) if(!(x)) __builtin_trap()
#define GLCall(x) GLClearError();\
    x;\
    GLLogCall(#x,__FILE__,__LINE__)

#endif

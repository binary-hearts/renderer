/*
 * terrain.cpp
 *
 *  Created on: 26-Jul-2023
 *      Author: surya
 */

#include "terrain.h"



Terrain::Terrain(glm::vec2 size, glm::ivec2 resolution, float rep) {
	glm::vec2 res(size.x/(resolution.x-1),size.y/(resolution.y-1));

	//Generate vertices
	for(float i = -(size.x/2); i <= size.x/2; i+=res.x){
		for(float j = -(size.y/2); j <= size.y/2; j+=res.y){
			VertexData t;
			t.position = glm::vec3(i,0,j);
	        t.tex_coord.x = (1 - (i + (size.x / 2)) / size.x)*rep;
	        t.tex_coord.y = (1 - (j + (size.y / 2)) / size.y)*rep;
			m_vertexData.push_back(t);
		}
	}

	for(int id = 0; id < m_vertexData.size() - resolution.y;id++){
		if((id+1)%(resolution.y) == 0) continue;
		GLuint idx[] = {id, id+1,id+resolution.y+1,id, id + resolution.y, id + resolution.y +1};
		m_indices.insert(std::end(m_indices), std::begin(idx), std::end(idx));
	}

    GLCall(glGenVertexArrays(1,&m_VAO));
    GLCall(glBindVertexArray(m_VAO));

    GLCall(glGenBuffers(1,&m_VBO));
    GLCall(glBindBuffer(GL_ARRAY_BUFFER,m_VBO));
    GLCall(glBufferData(GL_ARRAY_BUFFER,
						m_vertexData.size()*sizeof(VertexData),
						&m_vertexData[0],
						GL_STATIC_DRAW
					));
    GLCall(glGenBuffers(1,&m_IBO));
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_IBO));
    GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER,
						m_indices.size()*sizeof(GLuint),
						&m_indices[0],
						GL_STATIC_DRAW
					));

    GLCall(glEnableVertexAttribArray(0));
    GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*)0));
    GLCall(glEnableVertexAttribArray(1));
    GLCall(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void*)offsetof(VertexData,tex_coord)));

    GLCall(glBindVertexArray(0));
}

Terrain::~Terrain() {
	// TODO Auto-generated destructor stub
}


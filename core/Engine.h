#ifndef ENGINE_H
#define ENGINE_H

#include "Camera.h"
#include <iostream>
#include "glad/glad.h"
#include "Log.h"
#include "Window.h"
#include "Entity.h"
#include <imgui.h>


class Camera;


class Engine
{
    public:
        Engine(Window*);
        ~Engine();

        std::vector<Entity*> renderList;
        void setActiveCamera(Camera&);
        void render();
    protected:

    private:
        Window* m_window;
        Camera* m_activeCamera;
        Log  m_engineLog;
        float m_prevTime;
        void m_renderUI();
};

#endif // ENGINE_H

#include "BufferObject.h"
VertexBuffer::VertexBuffer(const void *data,unsigned int size){
    GLCall(glGenBuffers(1,&m_VBOID));
    GLCall(glBindBuffer(GL_ARRAY_BUFFER,m_VBOID));
    GLCall(glBufferData(GL_ARRAY_BUFFER,size,data,GL_STATIC_DRAW));
    this->unbind();
    has_data = true;
}

void
VertexBuffer::setData(const void *data,unsigned int size){
	this->bind();
    GLCall(glBufferData(GL_ARRAY_BUFFER,size,data,GL_STATIC_DRAW));
    this->unbind();
    has_data = true;
}

VertexBuffer::VertexBuffer(){
    glGenBuffers(1,&m_VBOID);
    has_data = false;
}

VertexBuffer::~VertexBuffer(){
	GLCall(glDeleteBuffers(1,&m_VBOID));
}


void
VertexBuffer::bind() const{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER,m_VBOID));
}

void
VertexBuffer::unbind() const{
    GLCall(glBindBuffer(GL_ARRAY_BUFFER,0));
}


//Index Buffer

IndexBuffer::IndexBuffer(GLuint *data,unsigned int count)
    :m_count(count)
{
    GLCall(glGenBuffers(1,&m_IBOID));
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_IBOID));
    GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER,count*sizeof(GLuint),data,GL_STATIC_DRAW));
    has_data = true;
}

IndexBuffer::IndexBuffer(){
    glGenBuffers(1,&m_IBOID);
    has_data=false;
    m_count = 0;
}

void
IndexBuffer::setData(const GLuint* data,unsigned int count){
	this->bind();
    GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER,count*sizeof(GLuint),data,GL_STATIC_DRAW));
    has_data = true;
}

IndexBuffer::~IndexBuffer(){
    GLCall(glDeleteBuffers(1,&m_IBOID));
}

void
IndexBuffer::bind() const{
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_IBOID));
}

void
IndexBuffer::unbind() const{
    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0));
}

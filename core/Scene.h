#ifndef SCENE_H
#define SCENE_H

#include <glm/vec3.hpp>
#include <vector>
#include "Entity.h"

class Scene
{
    private:
        std::vector<Entity*> m_objects;

    public:
        Scene();
        ~Scene();
        void addObject(Entity*);
        void removeObject(int id);
        std::vector<Entity*> getObjects();

    protected:

};

#endif // SCENE_H

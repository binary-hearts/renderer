#include "Shader.h"


Shader::Shader(const char filepath[])
:isRecompiled(false)
,m_bound(0)
,m_shaderCodePath(filepath)
,m_shaderFile(filepath)
,shaderlog("data/logs/shader.log")
{
    m_readOn = 0;
    if(!m_shaderFile.is_open()){
        shaderlog.error("Error opening shader file");
        shaderlog.error(m_shaderCodePath);
        exit(0);
    }
    m_vertexshader   = glCreateShader(GL_VERTEX_SHADER);
    m_fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
    compile();
}

void
Shader::compile(){

    //Check if file is modified before compiling 0 not compiled
    struct stat filestats;
    stat(m_shaderCodePath,&filestats);

    if(m_readOn < filestats.st_mtime){

        //reopen shader file if recompiling else read on will be 0
        if(m_readOn !=0 ){
        	m_shaderFile.close();
        	m_shaderFile.open(m_shaderCodePath);
        }
        m_readOn = filestats.st_mtime;
        isRecompiled = true;

        m_read();
        shaderlog.info("Reading shader done");
        shaderlog.info("Compiling shader");


        //Compile shader
        m_compileShader(m_vertexshader,vertexsource);
        m_compileShader(m_fragmentshader,fragmentsource);
        shaderlog.info("Compiling shader done");
        m_addProgram();
        shaderlog.info("Shader Program Bounded");

        //check if shader is already bound
        if(m_bound ==1){
            this->bindShader();
        }
    }
    else isRecompiled = false;
}

Shader::~Shader()
{
    m_shaderFile.close();
    GLCall(glDeleteProgram(ID));
}

void
Shader::m_compileShader(unsigned int shadertype,std::string& source){
	const char* srcPtr = source.c_str();
	GLCall(glShaderSource(shadertype,1,&srcPtr,NULL));
	GLCall(glCompileShader(shadertype));
	int success;
	char infolog[512];
	glGetShaderiv(shadertype,GL_COMPILE_STATUS,&success);
	if(!success){
        shaderlog.info(source.c_str());
        glGetShaderInfoLog(shadertype,512,NULL,infolog);
        shaderlog.error(infolog);
	}
}
void
Shader::m_addProgram(){
    int success;
    char infolog[512];
    ID  = glCreateProgram();
    glAttachShader(ID,m_vertexshader);
    glAttachShader(ID,m_fragmentshader);
    glLinkProgram(ID);
    glGetProgramiv(ID,GL_LINK_STATUS,&success);
    if(!success){
        glGetProgramInfoLog(ID,512,NULL,infolog);
        shaderlog.error(infolog);
    }
    glDeleteShader(m_vertexshader);
}

void
Shader::bindShader(){
    if(Shader::CurrentlyBound != nullptr){
        Shader::CurrentlyBound->unbindShader();
    }
    Shader::CurrentlyBound = this;
    glUseProgram(ID);
    m_bound = 1;
}
void
Shader::bindTexture(){
    for(int i = 0; i < m_textureList.size();i++){
        GLCall(glActiveTexture(GL_TEXTURE0 + i));
        GLCall(glBindTexture(GL_TEXTURE_2D,m_textureList[i]->getID()));
        setInt1(i,m_textureList[i]->getName().c_str());
    }
}

void
Shader::attachTexture(Texture* t){
	m_textureList.push_back(t);
}

void
Shader::unbindShader(){
    m_bound = 0;
    Shader::CurrentlyBound = nullptr;
}


void
Shader::m_read(){
    std::stringstream shaderStream;
    shaderStream << m_shaderFile.rdbuf();

    std::string data =  shaderStream.str();
    int vpos = data.find("$vertex");
    int fpos = data.find("$fragment");
    if(vpos < fpos){
    	vertexsource = data.substr(vpos + 7, fpos-vpos-7);
    	fragmentsource = data.substr(fpos + 9);
    }
    else{
    	fragmentsource = data.substr(fpos + 9, vpos-fpos-9);
    	vertexsource = data.substr(vpos + 7);
    }
}

//Uniforms
void
Shader::setFloat1(GLfloat value , const char name[]){
    this->bindShader();
    glUniform1f(glGetUniformLocation(ID,name),value);
    glUseProgram(ID);
}

void
Shader::setFloat2(GLfloat value1, GLfloat value2 ,const char name[]){
    this->bindShader();
    glUniform2f(glGetUniformLocation(ID,name),value1,value2);
    glUseProgram(ID);
}
void
Shader::setFloat3(GLfloat value1, GLfloat value2 , GLfloat value3,const char name[]){
    this->bindShader();
    glUniform3f(glGetUniformLocation(ID,name),value1,value2,value3);
    glUseProgram(ID);
}
void
Shader::setFloat4(GLfloat value1, GLfloat value2 , GLfloat value3, GLfloat value4 ,const  char name[]){
    this->bindShader();
    glUniform4f(glGetUniformLocation(ID,name),value1,value2,value3,value4);
    glUseProgram(ID);
}
void
Shader::setInt1  (GLint   value ,const char name[]){
    this->bindShader();
    glUniform1i(glGetUniformLocation(ID,name),value);
    glUseProgram(ID);
}
void
Shader::setInt2  (GLint   value1, GLint   value2 ,const char name[]){
    this->bindShader();
    (glUniform2i(glGetUniformLocation(ID,name),value1,value2));
    glUseProgram(ID);
}
void
Shader::setInt3  (GLint   value1, GLint   value2 , GLint   value3, const char name[]){
    this->bindShader();
    glUniform3i(glGetUniformLocation(ID,name),value1,value2,value3);
    glUseProgram(ID);
}
void
Shader::setInt4  (GLint   value1, GLint   value2 , GLint   value3, GLint   value4 , const char name[]){
    this->bindShader();
    glUniform4i(glGetUniformLocation(ID,name),value1,value2,value3,value4);
    glUseProgram(ID);
}

void
Shader::remove(){
    glDeleteProgram(ID);
}
void
Shader::setMatrix2(GLfloat *value , const char name[]){
    this->bindShader();
    glUniformMatrix2fv(glGetUniformLocation(ID,name),1,GL_FALSE,value);
    glUseProgram(ID);
}
void
Shader::setMatrix3(GLfloat *value ,const char name[]){
    this->bindShader();
    glUniformMatrix3fv(glGetUniformLocation(ID,name),1,GL_FALSE,value);
    glUseProgram(ID);
}
void
Shader::setMatrix4(GLfloat *value , const char name[]){
    this->bindShader();
    glUniformMatrix4fv(glGetUniformLocation(ID,name),1,GL_FALSE,value);
    glUseProgram(ID);
}

#include <stdio.h>

#include "Window.h"
#include "Camera.h"
#include "Engine.h"
#include "mesh.h"
#include "terrain.h"



class FPSCamera : public Camera{
public:
	FPSCamera(float width, float height,float FOV,Window* window,float yaw, float pitch)
	:Camera(width,height,FOV,window,yaw,pitch)
	{
		isWPressed = false;
		isAPressed = false;
		isQPressed = false;
		isEPressed = false;
		isSPressed = false;
		isDPressed = false;
		isShiftPressed = false;

		delta = 2;
	}
	void update(float time) override {
		if(m_window->mouseLocked == false){
			Camera::update(delta);
			return;
		}
		if(isShiftPressed){
			delta = 10*time;
		}
		else{
			delta = 2*time;
		}
		if(isWPressed){
	    	moveForward(delta);
		}
		if(isSPressed){
	    	moveBackward(delta);
		}
		if(isAPressed){
	    	moveLeft(delta);
		}
		if(isDPressed){
	    	moveRight(delta);
		}
		if(isQPressed){
	    	moveUp(delta);
		}
		if(isEPressed){
			moveDown(delta);
		}
		if(isDPressed){
	    	moveRight(delta);
		}
		Camera::update(delta);
	}
	float delta;
	bool isWPressed;
	bool isAPressed;
	bool isSPressed;
	bool isDPressed;
	bool isQPressed;
	bool isEPressed;
	bool isShiftPressed;
private:
};

int
main (int    argc,
      char **argv)
{
    Window mywindow(1280,720,"Mywindow");
    FPSCamera camera(1280,720,70,&mywindow,0,0);
    camera.setPosition(glm::vec3(0,2,0));
    camera.activate();
    Engine myengine(&mywindow);

    mywindow.addInputFunction(GLFW_KEY_W, GLFW_PRESS, [&camera]{
    	camera.isWPressed = true;
    });
    mywindow.addInputFunction(GLFW_KEY_S, GLFW_PRESS, [&camera]{
    	camera.isSPressed = true;
    });
    mywindow.addInputFunction(GLFW_KEY_Q, GLFW_PRESS, [&camera]{
    	camera.isQPressed = true;
    });
    mywindow.addInputFunction(GLFW_KEY_E, GLFW_PRESS, [&camera]{
    	camera.isEPressed = true;
    });
    mywindow.addInputFunction(GLFW_KEY_A, GLFW_PRESS, [&camera]{
    	camera.isAPressed = true;
    });
    mywindow.addInputFunction(GLFW_KEY_D, GLFW_PRESS, [&camera]{
    	camera.isDPressed = true;
    });
    mywindow.addInputFunction(GLFW_KEY_LEFT_SHIFT, GLFW_PRESS, [&camera]{
    	camera.isShiftPressed = true;
    });
    mywindow.addInputFunction(GLFW_KEY_W, GLFW_RELEASE, [&camera]{
    	camera.isWPressed = false;
    });
    mywindow.addInputFunction(GLFW_KEY_S, GLFW_RELEASE, [&camera]{
    	camera.isSPressed = false;
    });
    mywindow.addInputFunction(GLFW_KEY_A, GLFW_RELEASE, [&camera]{
    	camera.isAPressed = false;
    });
    mywindow.addInputFunction(GLFW_KEY_D, GLFW_RELEASE, [&camera]{
    	camera.isDPressed = false;
    });
    mywindow.addInputFunction(GLFW_KEY_Q, GLFW_RELEASE, [&camera]{
    	camera.isQPressed = false;
    });
    mywindow.addInputFunction(GLFW_KEY_E, GLFW_RELEASE, [&camera]{
    	camera.isEPressed = false;
    });
    mywindow.addInputFunction(GLFW_KEY_LEFT_SHIFT, GLFW_RELEASE, [&camera]{
    	camera.isShiftPressed = false;
    });


    mywindow.addMouseAction([&camera](double x, double y){
    	const float dup = 0.01;
    	static double prevx = 0, prevy = 0;
    	if(x!= prevx || y!= prevy){
    		float yaw = camera.getYaw() + (prevx-x)*dup;
    		camera.setYaw(yaw);
    		float pitch = camera.getPitch() + (prevy-y)*dup;
    		if(pitch < glm::radians(90.0) && pitch > glm::radians(-90.0)){
    			camera.setPitch(pitch);
    		}
    		prevx = x; prevy = y;
    	}
    });




    Shader  objShader("data/Shader/cube.glsl");
    Mesh Cube("data/models/cube.obj");
    Texture colors("data/Textures/cube.png","diffuse",GL_RGBA);
    Cube.setPosition(glm::vec3(0,1,0));
    objShader.attachTexture(&colors);
    Cube.AttachShader(&objShader);
    myengine.renderList.push_back(&Cube);

    Terrain myterrain(glm::vec2(100,100), glm::ivec2(500,500),1);
    Shader terrainShader("data/Shader/solid.glsl");
    Texture pave("data/Textures/roof_disp.png","displacement",GL_RED);
    Texture diffuse("data/Textures/roof.png","diffuse",GL_RGBA);
    terrainShader.attachTexture(&pave);
    terrainShader.attachTexture(&diffuse);
    myterrain.AttachShader(&terrainShader);

    myengine.renderList.push_back(&myterrain);
    myengine.setActiveCamera(camera);

    while(mywindow.isOpen()){
    	myengine.render();
    }

    return 0;
}

#include "texture.h"


Texture::Texture(const char path[],const char name[],GLenum mode_out)
  :m_name(name)
{
    //Texture wrap mode
    GLCall(glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_MIRRORED_REPEAT));
    GLCall(glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_MIRRORED_REPEAT));
    GLCall(glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR));
    GLCall(glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR));

    //Load and flip texture since opengl uses (0,0) as bottom left
    stbi_set_flip_vertically_on_load(true);
    unsigned char* m_data = stbi_load(path,&m_width,&m_height,&m_nchannels,0);

    //Generate texture object and bind
    GLCall(glGenTextures(1,&ID));
    GLCall(glBindTexture(GL_TEXTURE_2D,ID));

    //Give texture to opengl and generate mipmap if present
    if(m_data){
      GLCall(glTexImage2D(GL_TEXTURE_2D,0,m_getTextureFormat(m_nchannels),m_width,m_height,0,mode_out,GL_UNSIGNED_BYTE,m_data));
      GLCall(glGenerateMipmap(GL_TEXTURE_2D));
    }
    else{
      printf("Failed to load image");
    }

    //Freeup image after read
    stbi_image_free(m_data);
}

Texture::~Texture(){
    GLCall(glDeleteTextures(1,&ID));
}

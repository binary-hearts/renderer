#ifndef LTREE_H
#define LTREE_H

#include <string.h>
#include <string>
#include <vector>
#include <set>
#include <stdexcept>

#include <glm/glm.hpp>

#include "BufferObject.h"
#include "Entity.h"

#define PI 3.141593

class Ltree : public Entity{
    public:
        Ltree(std::string axiom, std::vector<std::string> rules, std::set<char> variables,int angle, int iter)
        {
            LDraw(axiom,variables,rules,angle,iter,1);

            GLCall(glGenVertexArrays(1,&m_VAO));
            GLCall(glBindVertexArray(m_VAO));
            m_VBO.setData(&m_Positions[0],m_Positions.size()*sizeof(glm::vec3));
            m_VBO.bind();

            GLCall(glEnableVertexAttribArray(0));
            GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0));

            m_VBO.unbind();
            GLCall(glBindVertexArray(0));

        }

        ~Ltree()
        {
            //dtor
        }

        ObjectType getObjectType(){
          return ObjectType::MESH;
        }
        void draw() override {
            GLCall(glBindVertexArray(m_VAO));
            GLCall(glDrawArrays(GL_LINES, 0, m_Positions.size()));
            GLCall(glBindVertexArray(0));
        }


    protected:
    private:
        unsigned int m_VAO;
        VertexBuffer m_VBO;


        std::vector<glm::vec3> m_Positions;
        void LDraw(std::string& axiom,std::set<char> variables,std::vector<std::string>& i_rules,float angle,int iterations,int line);
        void mDraw(std::string& axiom,std::set<char> variables,glm::vec3 current,int line,float angle_dif,float theta,std::vector<glm::vec3>& pos);
};

#endif // LTREE_H

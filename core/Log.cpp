#include "Log.h"

Log::Log(const char* path)
{
    setLevel(LevelEnum::ALL);
    logFile = fopen(path,"w");
}

Log::~Log()
{
    fclose(logFile);
}

void
Log::setLevel(LevelEnum level){
    this->level = level;
}

void
Log::warn(const char* description){
    if(level >=  LevelEnum::WARN){
        fprintf(logFile,"[WARN]:%s\n",description);
        fprintf(stderr ,"[WARN]:%s\n",description);
    }
}
void
Log::error(const char* description){
    if(level >=  LevelEnum::ERROR){
        fprintf(logFile,"[ERROR]:%s\n",description);
        fprintf(stderr ,"[ERROR]:%s\n",description);
    }
}
void
Log::info(const char* description){
    if(level == LevelEnum::ALL){
        fprintf(logFile,"[INFO]:%s\n",description);
        fprintf(stderr ,"[INFO]:%s\n",description);
    }
}

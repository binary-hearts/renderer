#include "Entity.h"

Entity::Entity()
:isHidden(false)
{
    static int LargestID = 1;
    this->ID = LargestID++;
    m_parent = nullptr;
    m_position = m_rotation = m_scale = glm::vec3(0.0f);
}

Entity::~Entity()
{
    //dtor
}



void
Entity::addChild(Entity* obj){
    this->m_children.push_back(obj);
    obj->m_parent = this;
}
void
Entity::removeChild(Entity* obj){
    for(auto i = m_children.begin(); i != m_children.end(); ++i){
    	if(*i == obj){
    		m_children.erase(i);
    		return;
    	}
    }

}





void
Entity::setPosition(glm::vec3 position){
    m_position = position;
}

void
Entity::setRotation(glm::vec3 rotation){
    m_rotation = rotation;
}

void
Entity::setScale(glm::vec3 scale){
    m_scale = scale;
}

glm::vec3
Entity::getPosition(){
    return m_position;
}
glm::vec3
Entity::getRotation(){
    return m_rotation;
}

glm::vec3
Entity::getScale(){
    return m_scale;
}

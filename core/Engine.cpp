#include "Engine.h"

Engine::Engine(Window* window)
:m_engineLog("data/logs/engine.log")
,m_window(window)
,m_activeCamera(nullptr)
{
    m_engineLog.setLevel(LevelEnum::ALL);
    m_prevTime = glfwGetTime();

}

Engine::~Engine()
{
    //dt
}


void
Engine::render(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    float time = glfwGetTime();
    float deltatime = time - m_prevTime;
    m_prevTime = time;
    for(int it = 0; it < renderList.size(); it++){
        if(renderList[it]->getObjectType() == ObjectType::MESH){
        	renderList[it]->shader->compile();
			renderList[it]->shader->bindShader();
			m_activeCamera->update(deltatime);
			renderList[it]->shader->bindTexture();

			renderList[it]->shader->setFloat2(m_window->width,m_window->height,"uResolution");
			renderList[it]->shader->setFloat1(m_window->getTime(),"uTime");

			glm::mat4 projection,view;
			{//Camera update
		        projection = m_activeCamera->getProjectionMatrix();
		        view = m_activeCamera->getWorldToViewMatrix();
			}
			glm::mat4 model(1.0f);
			glm::vec3 pos = renderList[it]->getPosition();
			model = glm::translate(model,pos);
			model = glm::rotate(model, glm::radians(renderList[it]->getRotation().x), glm::vec3(1,0,0));
			model = glm::rotate(model, glm::radians(renderList[it]->getRotation().y), glm::vec3(0,1,0));
			model = glm::rotate(model, glm::radians(renderList[it]->getRotation().z), glm::vec3(0,0,1));
			model = projection*view*model;
			renderList[it]->shader->setMatrix4(glm::value_ptr(model),"mvp");

			m_renderUI();

			renderList[it]->draw();
			renderList[it]->shader->unbindShader();
        }
    }
    m_window->swapBuffers();
}

void
Engine::m_renderUI(){
	if(m_window->mouseLocked == true) return;
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    ImGui::Begin("Stock Data Analysis");
    ImGui::Text("Select Stock:");
    ImGui::End();
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void
Engine::setActiveCamera(Camera& c){
    m_activeCamera = &c;
}

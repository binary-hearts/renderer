#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Shader.h"
#include "Entity.h"
#include "Window.h"


class Camera : public Entity{
  private:
    glm::mat4   m_projection,m_view;
    float       m_FOV;
    bool        m_active;
    glm::vec3   m_target,m_up;
    float m_yaw,m_pitch;


    void draw() override {
        return;
    }
    ObjectType getObjectType() override {
        return ObjectType::CAMERA;
    }



  public:
    Camera(float width, float height,float FOV,Window* window,float yaw, float pitch);
    ~Camera();
    glm::mat4 getWorldToViewMatrix();
    glm::mat4 getProjectionMatrix();
    void activate();
    glm::vec3 getTarget();
    void      setTarget(glm::vec3);
    void      setYaw(float yaw) { this->m_yaw = yaw;}
    void      setPitch(float pitch) { this->m_pitch = pitch;}
    float     getYaw(){return m_yaw;}
    float     getPitch(){return m_pitch;}

    void update(float) override;
    void moveForward(float distance);
    void moveBackward(float distance);
    void moveLeft(float distance);
    void moveRight(float distance);
    void moveUp(float distance);
    void moveDown(float distance);
	Window*& getWindow();
	void setWindow(Window *mWindow);

  protected:
    Window* m_window;
};


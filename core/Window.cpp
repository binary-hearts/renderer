#include "Window.h"

void GLAPIENTRY
MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam )
{
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
  exit(0);
}

Window::Window(int w,int h,char title[])
    :m_windowLog("data/logs/window.log")
    ,width(w)
    ,height(h)
    ,m_title(title)
	,mouseLocked(true)
{
    glfwSetErrorCallback(s_errorcallback);
    if(glfwInit())
    {
        m_windowLog.info("Initialized Glfw success");
    }
    else
    {
        m_windowLog.error("Initialized Glfw failed");
    }

    GLFWmonitor* primaryMonitor = glfwGetPrimaryMonitor();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);

    // Get the monitor's video mode to determine its resolution
    const GLFWvidmode* mode = glfwGetVideoMode(primaryMonitor);

    width = mode->width;
    height = mode->height;
    m_window = glfwCreateWindow(mode->width,mode->height,title,primaryMonitor,NULL);
    //m_window = glfwCreateWindow(m_width,m_height,title,NULL,NULL);
    if(!m_window)
    {
        m_windowLog.error("Window creation failed\n");
    }
    glfwMakeContextCurrent(m_window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

    //Setup IMGUI
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(m_window, true);
    ImGui_ImplOpenGL3_Init("#version 460");


    callbacks();
    glfwSetCursorPos(m_window, width/2,height/2);
    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    //glEnable              ( GL_DEBUG_OUTPUT );
    glDebugMessageCallback( MessageCallback, 0 );
    this->addInputFunction(GLFW_KEY_LEFT_CONTROL, GLFW_PRESS, [this]{
    		if(this-> mouseLocked){
    			glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    			mouseLocked = false;
    		}
    		else{
    			glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    			mouseLocked = true;
    		}
    });
}

Window::~Window()
{
    glfwDestroyWindow(m_window);
    glfwTerminate();
}

void 
Window::s_framebuffersize(GLFWwindow* window,int width,int height)
{
  glViewport(0,0,width,height);
  Window *this_window = (Window*)glfwGetWindowUserPointer(window);
  this_window->width = width;
  this_window->height = height;
}
void
Window::callbacks()
{
    glfwSetKeyCallback(m_window,s_keycallback);
    glfwSetFramebufferSizeCallback(m_window,s_framebuffersize);
    glfwSetWindowUserPointer(m_window,this);
    glfwSetCursorPosCallback(m_window, s_mouseCursorCallback);
}

bool
Window::isOpen()
{
    return !glfwWindowShouldClose(m_window);
}

void
Window::swapBuffers(int interval)
{
    glfwSwapBuffers(m_window);
    glfwSwapInterval(interval);
    glfwPollEvents();
}

float
Window::getTime(){
  return glfwGetTime();
}


void
Window::addInputFunction(int key,int action,std::function<void()> call_func){
  m_inputs.emplace_back(key,action,call_func);
}


void 
Window::s_keycallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
      glfwSetWindowShouldClose(window, GLFW_TRUE);

  Window *this_window = (Window*)glfwGetWindowUserPointer(window);
  std::vector<Inputfunction> inputs = this_window->m_inputs;
  for(int i = 0; i < inputs.size();i++){
    if( key == inputs[i].key  && action == inputs[i].action) inputs[i].callFunction();
  }
}
void
Window::s_mouseCursorCallback(GLFWwindow* window, double xpos, double ypos){
	Window *this_window = (Window*)glfwGetWindowUserPointer(window);
	for(auto& f : this_window->m_mouseActions){
		f(xpos,ypos);
	}
}

void
Window::Info(){
  printf("Title: %s\nWidth: %f\nHeight:%f\n",m_title,width,height);
}

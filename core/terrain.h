/*
 * terrain.h
 *
 *  Created on: 26-Jul-2023
 *      Author: surya
 */

#include <vector>
#include <glm/glm.hpp>
#include "BufferObject.h"
#include "Entity.h"

#ifndef TERRAIN_H_
#define TERRAIN_H_
    struct VertexData{
    	glm::vec3 position;
    	glm::vec2 tex_coord;
    };


class Terrain : public Entity {
public:
	Terrain(glm::vec2 size, glm::ivec2 resolution, float tex_repetition);
	virtual ~Terrain();

	void draw(){
	    GLCall(glBindVertexArray(this->m_VAO));
	    GLCall(glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT,0));
	    GLCall(glBindVertexArray(0));
	}
	ObjectType getObjectType() override{
		return ObjectType::MESH;
	}
    void AttachShader(Shader* shader){
    	this->shader = shader;
    }
private:

	std::vector<VertexData> m_vertexData;
	std::vector<GLuint> m_indices;
    unsigned int m_VAO;
    unsigned int m_VBO;
    unsigned int m_IBO;
};


#endif /* TERRAIN_H_ */

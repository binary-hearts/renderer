#include "mesh.h"



Mesh::Mesh(const char filepath[])
:m_filePath(filepath)
,m_VBO()
{
    m_file = fopen(filepath,"r");
    if(m_file == NULL){
        fprintf(stderr,"\nUnable to open obj file!");
        fprintf(stderr,"\n%s",strerror(errno));
    }
    else{
        m_loadOBJ();
        m_setupMesh();
    }
}


void
Mesh::m_loadOBJ(){
    //Loads Obj file
    //
    //Declare temperory vectors to store position normal and uv
    std::vector<glm::vec3> tpos;
    std::vector<glm::vec3> tnorms;
    std::vector<glm::vec2> tuvs;

    //letter = first character of each line
    char letter[3];
    while((fscanf(m_file,"%2s", letter) > 0)){
        if( strcmp(letter,"v") == 0  ){
            glm::vec3 v;
            fscanf(m_file,"%f %f %f",&v.x,&v.y,&v.z);
            tpos.push_back(v);
        }
        else if( strcmp(letter,"vn") == 0  ){
            glm::vec3 vn;
            fscanf(m_file,"%f %f %f",&vn.x,&vn.y,&vn.z);
            tnorms.push_back(vn);
        }
        else if( strcmp(letter,"vt") == 0  ){
            glm::vec2 vt;
            fscanf(m_file,"%f %f",&vt.x,&vt.y);
            tuvs.push_back(vt);
        }
        else if( strcmp(letter,"f") == 0  ){
            char t;
            int count = 0;
            while((t = getc(m_file)) != '\n'){
                int v,vn,vt;
                fscanf(m_file,"%d/%d/%d",&v,&vt,&vn);
                Vertex vert;
                vert.position = tpos[v-1];
                vert.normal   = tnorms[vn-1];
                vert.texCoord = tuvs[vt-1];
                this->vertices.push_back(vert);
                count++;
            }
            if(count > 3) printf("Not a triangulated geometry, Quads and Ngons not supported yet");
        }
        else{
            //if not any supported input then just read line to seek next line
            char temp;
            while((temp = getc(m_file)) != '\n');
        }
    }
}

void
Mesh::m_setupMesh(){
    GLCall(glGenVertexArrays(1,&m_VAO));
    GLCall(glBindVertexArray(m_VAO));
    m_VBO.setData(&vertices[0],vertices.size()*sizeof(Vertex));
    m_VBO.bind();

    GLCall(glEnableVertexAttribArray(0));
    GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0));

    GLCall(glEnableVertexAttribArray(1));
    GLCall(glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex,normal)));

    GLCall(glEnableVertexAttribArray(2));
    GLCall(glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex,texCoord)));

    m_VBO.unbind();
    GLCall(glBindVertexArray(0));

}

Mesh::~Mesh(){
    fclose(m_file);
}

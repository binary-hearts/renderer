#ifndef TEXTURE_H
#define TEXTURE_H

#include "error.h"
#include "glad/glad.h"
#include <string>
#include "stb_image.h"


///Loads Texture Files <2D Images>
class Texture{
  public:
    /**Constructor <file path of texture, `InputMode, `OutputMode
        Mode => GL_RGBA(default) , GL_RED, GL_RG, GL_RGB, GL_BGR, GL_BGRA,[+_INTEGER]
                GL_STENCIL_INDEX, GL_DEPTH_COMPONENT, GL_DEPTH_STENCIL
    */
    Texture(const char path[],const char name[],GLenum mode_out = GL_RGBA);

    ~Texture();
    std::string getName(){
    	return m_name;
    }
    GLuint getID(){
    	return ID;
    }

  private:
    GLenum m_getTextureFormat(int numChannels) {
        if (numChannels == 1) {
            return GL_RED;
        } else if (numChannels == 3) {
            return GL_RGB;
        } else if (numChannels == 4) {
            return GL_RGBA;
        }
        // Handle unsupported channel count (you can add more cases if needed)
        return GL_RGBA;
    }
    GLuint ID;
    int m_width,m_height,m_nchannels;
    std::string m_name;
};


#endif


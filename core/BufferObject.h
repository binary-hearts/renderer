#ifndef VERTEXBUFFEROBJECT_H
#define VERTEXBUFFEROBJECT_H

#include "glad/glad.h"
#include "error.h"
#include "Log.h"

class VertexBuffer{
    public:
        VertexBuffer(const void *data,unsigned int size);
        VertexBuffer();
        ~VertexBuffer();
        void bind() const;
        void unbind() const;
        void setData(const void *data,unsigned int size);

    private:
        bool has_data;
        GLuint m_VBOID;
    protected:
};

class IndexBuffer{
    public:
        IndexBuffer(GLuint *data,unsigned int size);
        IndexBuffer();
        ~IndexBuffer();
        void bind() const;
        void unbind() const;
        inline GLuint getCount() { return m_count; }
        void setData(const GLuint* data, unsigned int count);

    private:
        bool has_data;
        GLuint m_count;
        GLuint m_IBOID;

    protected:
};


#endif

#include "Ltree.h"



void
Ltree::mDraw(std::string& axiom,std::set<char> variables,
           glm::vec3 current,
           int line,float angle_dif,float theta,
           std::vector<glm::vec3>& pos) {
    if(axiom.size() == 0) return;

    float rad;
    switch(axiom[0]) {
    case '+':
        //Increment angle
        theta += angle_dif;
        break;

    case '-':
        //Decrement angle
        theta = theta - angle_dif;
        break;

    case '[':
        axiom.erase(0,1);
        mDraw(axiom,variables,current,line,angle_dif,theta,pos);
        pos.push_back(current);

        mDraw(axiom,variables,current,line,angle_dif,theta,pos);
        pos.emplace_back(current);
        return;
        break;

    case ']':
        axiom.erase(0,1);
        return;
        break;

    default:
        for(char i :variables){
            if( i == axiom[0] ){
                rad = theta*(PI/180);
                current += glm::vec3(glm::cos(rad)*line,glm::sin(rad)*line,0);
                pos.push_back(current);

                break;
            }
        }
    }
    axiom.erase(0,1);
    mDraw(axiom,variables,current,line,angle_dif,theta,pos);
}


void
Ltree::LDraw(std::string& axiom,
                             std::set<char> variables,
                             std::vector<std::string>& i_rules,
                             float angle,int iterations,int line = 1)
                             {
    //Check if Rules are valid
    for(int i = 0; i < (int)i_rules.size();i++){
        std::string rule = i_rules[i];
        int eq = 0;
        for(int j=0; j < (int)rule.size(); j++) {
            if(rule[j] == '=') eq++;
        }
        if(eq != 1) throw std::logic_error("Rule must have only one equal \"=\" sign:"+rule);

        eq = 0;
        int j = 0;
        char x;
        while((x = rule[j++]) != '='){
            if(x != ' '){
                eq++;
            }
        }
        if(eq != 1) throw std::logic_error("LHS Should be a single character");
    }

    //Seperate rhs and lhs
    struct Rules{
        char lhs;
        std::string rhs;
    };

    std::vector<Rules> lrules;

    for(int j = 0; j < (int)i_rules.size(); j++) {
            std::string rule = i_rules[j];
            Rules r;
            int lpos = rule.find('=');
            r.lhs = rule[0];
            r.rhs = rule.substr(lpos+1);
            lrules.push_back(r);
    }


    std::string res;
    //Replace all occurence of LHS to RHS
    for(int i = 0; i < iterations; i++) {
        int  pos  = 0;
        while(pos < (int)axiom.size()) {
            bool rule_matched = false;

            for(int j = 0; j < (int)lrules.size(); j++){
                if( axiom[pos] == lrules[j].lhs ) {
                    res.append(lrules[j].rhs);
                    pos += 1;
                    rule_matched = true;

                }
            }
            if(!rule_matched){
                //else append whaterver that found to string
                res.push_back(axiom[pos]);
                pos++;
            }
        }
        axiom = res;
        res = "";
    }

    m_Positions.push_back(glm::vec3(0,0,0));
    mDraw(axiom,variables,glm::vec3(0,0,0),line,angle,90,m_Positions);
}













#ifndef LOG_H
#define LOG_H

#include<cstdio>

enum class LevelEnum{
    NONE,    //0
    ERROR,   //1
    WARN,    //2
    ALL      //3
};
class Log
{
    public:
        Log(const char* path);
        ~Log();
        void setLevel(LevelEnum level);
        void warn (const char* description);
        void error(const char* description);
        void info (const char* description);
        FILE* logFile;


    protected:

    private:
        LevelEnum level;
};

#endif // LOG_H

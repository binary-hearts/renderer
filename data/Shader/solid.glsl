$vertex
#version 460 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 TexCoord;
uniform sampler2D displacement;

uniform mat4 mvp;

void main()
{
    vec4 pos = vec4(aPos.xyz,1.0);
    vec4 disp = texture(displacement,aTexCoord);
    float dip = disp.r;
    gl_Position = mvp * (pos+vec4(0.0,dip,0.0,0.0));
    TexCoord = aTexCoord;
}

$fragment
#version 330 core
uniform sampler2D diffuse;
uniform vec2 uResolution;

in vec2 TexCoord;
out vec4 FragColor;


void main()
{
	//FragColor = vec4(TexCoord.xy,0.0,1.0);
	FragColor = texture(diffuse,TexCoord);
}

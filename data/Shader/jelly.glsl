$vertex
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 TexCoord;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
void main(){
   gl_Position = u_projection * u_view * u_model * (vec4(aPos.x, aPos.y, aPos.z, 1.0));
   TexCoord = aTexCoord;
}

$fragment
#version 330 core
in vec2 TexCoord;
out vec4 FragColor;

uniform float uTime;
uniform vec2 uResolution;

#define xsquish 4.0 // (sin(uTime * 0.5)  * 2.0 + 2.0)
#define ysquish 2.0 // (sin(uTime * 0.75) * 2.0 + 2.0)

float time;
#define time_scale float(1.5);

struct wave {
    float f;
    float a;
    float s;
};

wave waves[] = wave[](
    wave(5.0, 500.0, 3.0),
    wave(7.0, 1200.0, 6.0),
    wave(18.0, 1200.0, -6.0)
);

float get_waves(float value, wave waves[3]) {
    float sum = 0.0;
    for (int n = 0; n < waves.length(); n += 1) {
        wave h = waves[n];
        sum += sin(value * h.f + time * h.s) / h.a;
    }
    return sum;
}

float move(float speed, float amount) {
    float value = sin(time * speed) * 0.5 + 0.5;
    return 1.0 + value * amount;
}

#define sqr(x) (x*x)

float rand(vec3 value) {
    value = sin(value);
    float random = dot(value, vec3(12.9898, 78.233, 37.719));
    random = fract(sin(random) * 143758.5453);
    return random;
}

void main() {
    vec2 pure_uv = TexCoord  - 0.5;
    pure_uv.x *= uResolution.x / uResolution.y;

    pure_uv *= 1.4;

    #define pixels float(96)
    pure_uv = floor(pure_uv * pixels) / pixels;
    time = uTime * time_scale;

    vec2 uv = pure_uv;
    uv.y *= 0.9;
    uv.x =  tanh(uv.x * xsquish) / xsquish;
    uv.y = atanh(uv.y * ysquish) / ysquish;

    FragColor = vec4(0.0, 0.15, 0.2, 1.0) * (1.0 - length(pure_uv) / 2.0);

    float tendrils[5] = float[](
        0.000, 0.768, 0.983, 0.491, -0.353
    );

    for (int n = 0; n < tendrils.length(); n += 1) {
        float r = tendrils[n];
        float position = uv.x + 0.05 * float(n) - 0.095;
        float a = get_waves(uv.y * 10.0 + r * 4.0, waves) * 250.0;
        float value = 1.0 - max(0.0, smoothstep(a, a+0.9, position * 50.0)
                                   + smoothstep(a, a-0.9, position * 50.0));

        if (value > 0.5) value = 1.0;
        else value = 0.0;

        FragColor += value * max(0.05 - uv.y + r / 24.0, 0.0)
                                 * max(sqr(uv.y + r / 9.0 + 0.8), 0.0)
                                 * 5.0;
    }
    uv = pure_uv;

    float theta = atan(uv.x, uv.y);
    float radius = length(uv);

    radius += get_waves(theta, waves) * 1.5;

    float mirrored_x = uv.x;
    if (mirrored_x < 0.5) {
        mirrored_x += 0.5 - mirrored_x;
    }
    float mirrored_theta = atan(mirrored_x, uv.y);
    radius += sin(mirrored_theta * 10.0 - time * 4.0) / 50.0;

    float threshold = 0.2;
    float outer = sqr(sqr(sqr(radius * 4.5)));
    float inner = (1.0 - radius) / 2.0;
    if (radius < threshold && uv.y > 0.0 + get_waves(uv.x, waves) * 8.0) {
        float r = inner * 0.2 + outer * move(0.2723, 1.0);
        float g = inner * 0.3 + outer * move(0.224, 1.0);
        float b = inner * 0.4 + outer * move(0.3151, 1.0);

        FragColor *= sqr(inner);
        FragColor += vec4(r, g, b, 1);
    }

    uv = pure_uv;
    float noise = rand(vec3(uv, mod(time, 10.0))) * 0.01;
    FragColor += noise;
}

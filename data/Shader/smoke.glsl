$vertex
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 TexCoord;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
void main(){
   gl_Position = u_projection * u_view * u_model * (vec4(aPos.x, aPos.y, aPos.z, 1.0));
   TexCoord = aTexCoord;
}

$fragment

#version 330 core
out vec4 FragColor;

in vec2 TexCoord;
vec2  uResolution = vec2(1.0f,1.0f);
uniform float   uTime;


float rand(vec2 n) {
    return fract(cos(dot(n, vec2(16.9898, 10.1414))) * 93758.5453);
}

float noise(vec2 n) {
    const vec2 d = vec2(0.0, 1.0);
    vec2 b = floor(n), f = smoothstep(vec2(0.0), vec2(1.0), fract(n));
    return mix(mix(rand(b), rand(b + d.yx), f.x), mix(rand(b + d.xy), rand(b + d.yy), f.x), f.y);
}

float fbm(vec2 n) {
    float total = 0.0, amplitude = 1.0;
    for (int i = 0; i < 10; i++) {
        total += noise(n) * amplitude;
        n += n;
        amplitude *= 0.3;
    }
    return total;
}

void main() {
    const vec3 c1 = vec3(300.0/255.0, 50.0/255.0, 197.0/255.0);
    const vec3 c2 = vec3(-11.0/255.0, 50.0/255.0, 111.4/255.0);
    const vec3 c3 = vec3(0.2 + .19, 0.19, 0.19);
    const vec3 c4 = vec3(6./255.0, 150.0/255.0, 260./255.0);
    const vec3 c5 = vec3(0.6);
    const vec3 c6 = vec3(.3);

    vec2 p = TexCoord.xy * 7.0 / uResolution.xx;
    float q = fbm(p - uTime * 0.3);
    vec2 r = vec2(fbm(p + q + uTime * 1.0 - p.x - p.y), fbm(p + q - uTime * 1.0));
    vec3 c = mix(c1, c2, fbm(p + r)) + mix(c3, c4, r.x) - mix(c5, c6, r.y);
    FragColor = vec4(c * cos(0.0 * TexCoord.y / uResolution.y), 1.0);
    FragColor.w = 0.8;
}

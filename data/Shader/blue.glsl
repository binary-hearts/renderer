$vertex
#version 330 core
layout (location = 0) in vec3 aPos;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

void main(){
   gl_Position = u_projection * u_view * u_model * (vec4(aPos.x, aPos.y, aPos.z, 1.0));
}

$fragment
#version 330 core

out vec4 FragColor;

void main()
{
    // Set the output color to a solid red color (RGBA: 1.0, 0.0, 0.0, 1.0)
    FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}

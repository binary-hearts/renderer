$vertex
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 TexCoord;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
void main(){
   gl_Position = u_projection * u_view * u_model * (vec4(aPos.x, aPos.y, aPos.z, 1.0));
   TexCoord = aTexCoord;
}

$fragment

#version 330 core
out vec4 FragColor;

in vec2 TexCoord;
uniform vec2      uResolution;
uniform float     uTime;
uniform sampler2D wall;

void main(void){
	float time = uTime;
	vec2 resolution = vec2(1.0f,1.0f);
	vec3 destColor = vec3(0.52, 0.2, 0.1);
	vec2 p = (TexCoord.xy * 2.0 - resolution) / min(resolution.x, resolution.y);
	float a = atan(p.y / p.x) * 2.0; // Instead of * 2.0, try * 26 or * 128 and higher
	float l = 0.05 / abs(length(p) - 1. + sin(a + time * 4.5) * 0.1);
	destColor *= 1.+ cos(a + time * 00.18) * 0.2;

	vec3 destColor2 = vec3(0.8, 0.1, 0.21);
	vec2 p2 = (TexCoord.xy * 3.0 - resolution) / min(resolution.x, resolution.y);
	float a2 = atan(p.y / p.x) * 3.0;
	float l2 = 0.09 / abs(length(p) + 0.1 - (cos(time/1.)+0.1) + sin(a + time * 15.1) * ((0.5 * l))*l+l+l+l+l);
	destColor2 *= ( 0.8 + sin(a + time * 00.03) * 0.03 ) * 4.0;

	vec3 destColor3 = vec3(0.2, 0.1, 0.35);
	vec2 p3 = (TexCoord.xy * 2.0 - resolution) / max(resolution.x, resolution.y);
	float a3 = atan(p.y / p.x) * 500.0;
	float l3 = 0.1 / abs(length(p) - 0.4 + sin(a + time * 1.5) * (1. * l2*l));
	destColor3 *= 1.5 + sin(a + time * 10.23) * 0.03;

	//FragColor = texture(wall,TexCoord);
    FragColor = vec4(l*destColor + l2*destColor2 + l3*destColor3, 0.1);
}

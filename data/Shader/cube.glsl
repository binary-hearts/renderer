$vertex
#version 460 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;
out vec2 TexCoord;

uniform mat4 mvp;
void main()
{
    gl_Position = mvp * (vec4(aPos.xyz,1.0));
    TexCoord = aTexCoord;
}

$fragment
#version 330 core
uniform sampler2D diffuse;
uniform vec2 uResolution;

in vec2 TexCoord;
out vec4 FragColor;


void main()
{
	FragColor = texture(diffuse,TexCoord);
}

$vertex
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 TexCoord;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
void main(){
   gl_Position = u_projection * u_view * u_model * (vec4(aPos.x, aPos.y, aPos.z, 1.0));
   TexCoord = aTexCoord;
}

$fragment

#version 330 core

in vec2 TexCoord;
out vec4 FragColor;

uniform float uTime;
uniform vec2 uResolution;

void main(){
    vec2 uv =  (2.0 * (TexCoord*uResolution) - uResolution.xy) / min(uResolution.x, uResolution.y);
   
    for(float i = 1.0; i < 8.0; i++){
    uv.y += i * 0.1 / i * 
      sin(uv.x * i * i + uTime * 0.5) * sin(uv.y * i * i + uTime * 0.5);
  }
    
   vec3 col;
   col.r  = uv.y - 0.1;
   col.g = uv.y + 0.3;
   col.b = uv.y + 0.95;
    
    FragColor = vec4(col,1.0);
}

$vertex
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 TexCoord;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
void main(){
   gl_Position = u_projection * u_view * u_model * (vec4(aPos.x, aPos.y, aPos.z, 1.0));
   TexCoord = aTexCoord;
}

$fragment
#version 330 core
in vec2 TexCoord;
out vec4 FragColor;

uniform float uTime;
uniform vec2 uResolution;

// philip.bertani@gmail.com


#define oct 5   //number of fbm octaves
#define pi  3.14159265

float random(vec2 p) {
    //a random modification of the one and only random() func
    return fract( sin( dot( p, vec2(12., 90.)))* 1e5 );
}


//this is taken from Visions of Chaos shader "Sample Noise 2D 4.glsl"
float noise(vec3 p) {
    vec2 i = floor(p.yz);
    vec2 f = fract(p.yz);
    float a = random(i + vec2(0.,0.));
    float b = random(i + vec2(1.,0.));
    float c = random(i + vec2(0.,1.));
    float d = random(i + vec2(1.,1.));
    vec2 u = f*f*(3.-2.*f);
    
    return mix(a,b,u.x) + (c-a)*u.y*(1.-u.x) + (d-b)*u.x*u.y;

}

float fbm3d(vec3 p) {
    float v = 0.;
    float a = .5;
    vec3 shift = vec3(100.);  //play with this
    
    float angle = pi/4.;      //play with this
    float cc=cos(angle), ss=sin(angle); 
    mat3 rot = mat3( cc,  0., ss, 
                      0., 1., 0.,
                     -ss, 0., cc );
    for (int i=0; i<oct; i++) {
        v += a * noise(p);
        p = rot * p * 2. + shift;
        a *= .67;  //changed from the usual .5
    }
    return v;
}

mat3 rxz(float an){
    float cc=cos(an),ss=sin(an);
    return mat3(cc,0.,-ss,
                0.,1.,0.,
                ss,0.,cc);                
}
mat3 ryz(float an){
    float cc=cos(an),ss=sin(an);
    return mat3(1.,0.,0.,
                0.,cc,-ss,
                0.,ss,cc);
}   

vec3 get_color(vec3 p) {
    vec3 q;
    q.x = fbm3d(p);
    q.y = fbm3d(p.yzx);
    q.z = fbm3d(p.zxy);

    float f = fbm3d(p + q);
    
    return q*f;
}

void main()
{

    float tt = uTime / 8.;
    vec2 uv = (2.*(TexCoord*uResolution)-uResolution.xy)/uResolution.y;
    vec2 mm = vec2(1.7,0.); // (2.*iMouse.xy-uResolution.xy)/uResolution.y;

    vec3 rd = normalize( vec3(uv, -2.) );  
    vec3 ro = vec3(0.,0.,0.);
    
    float delta = 2.*pi/10.;
    //float initm = -.5 * delta;
    mat3 rot = rxz(-mm.x*delta) * ryz(-mm.y*delta);
    
    float timefac = 1.;
    ro -= rot[2]*uTime/timefac;
    
    rd = rot * rd;
    vec3 orig_rd = rd;
    
    vec3 p = ro + rd;
    
    vec3 cc = vec3(0.);

    float stepsize = .05;
    float totdist = stepsize;
    
    for (int i=0; i<20; i++) {
       vec3 cx = get_color(p);
       p += stepsize*rd;
       float fi = float(i);
       cc += exp(-totdist*totdist*2.)* cx;
       totdist += stepsize;
       rd = ryz(.5)*rd;   //yz rotation here
    }
    
    
    vec3 x2 = ro + vec3(10.,-1.,-5.), x1= ro + vec3(-10.,2.,0.);
    vec3 x0 = p + length(cc)*orig_rd;
    
    //laser (kind of) here
    float dist_to_line = length( cross( (x0-x1), (x0-x2) ) ) / length( x2 - x1 );
    float dd = dist_to_line * dist_to_line;
    float extra_lum = exp(-dd*dd/10.);
    float extra_lum2 = exp(-dd/200.);
    float extra_lum3 = exp(-dd/5.);
    
    cc.g += 5.*extra_lum;
    cc.g += extra_lum2;
    cc.r += extra_lum2;
    cc.b += 2.* extra_lum2;
    cc.b += 2.*extra_lum3; cc.r += 3.*extra_lum3;  //add some more color to center
    
    //cc = .5 + 1.2*(cc-.5);  
    cc = pow( cc/9. , vec3(4.));   

    FragColor = vec4(cc,1.0);
    
    
}
